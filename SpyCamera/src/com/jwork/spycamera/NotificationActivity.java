package com.jwork.spycamera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.jwork.spycamera.utility.LogUtility;

public class NotificationActivity extends Activity {

    private LogUtility log;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = LogUtility.getInstance();
        log.v(this, "onCreate");
        setContentView(R.layout.activity_notification);
		Intent intent = new Intent(this, CameraTaskService.class);
		stopService(intent);
		finish();
    }

}